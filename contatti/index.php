<?php

require_once ('../funzioni.php');
lugheader ('Italian Linux Planet', 'News dal mondo freesoftware in Italia');

?>

<div class="container main-contents">
	<div class="row">
		<div class="col">
			<p>
				Dubbi? Quesiti? Suggerimenti? Critiche?
			</p>
			<p>
				Per comunicazioni di ogni tipo e genere relativi a Planet LUG e Planet ILS manda una mail a <a href="mailto:webmaster@linux.it">webmaster@linux.it</a>
			</p>
			<p>
				Tieni comunque sempre presente che i feeds indicizzati su <a href="/lug">Planet LUG</a> sono rigorosamente ed esclusivamente tratti dai contenuti della LugMap e periodicamente allineati in modo automatico, dunque su questo aggregatore non sono ammesse altre fonti che non siano LUG/FSUG/gruppi informali/associazioni di promozione a Linux ed al software libero. Se vuoi dare visibilità ad un sito che non rientra in questa definizione prendi in considerazione l'opportunità di diventare socio ILS ed essere aggregato in <a href="/ils">Planet ILS</a>.
			</p>
		</div>
	</div>
</div>

<div style="clear: both; margin-bottom: 20px"></div>

<?php lugfooter (); ?>

