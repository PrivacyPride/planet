# Planet LUG

Planet LUG è un aggregatore di feeds RSS, che colleziona le news
provenienti dai siti dei LUG e dei soci dell'associazione Italian Linux
Society.

L'elenco dei feeds inclusi viene periodicamente e automaticamente
estrapolato dalla LugMap e dal gestionale dei soci ILS.

Visita il sito [https://planet.linux.it](https://planet.linux.it/)

## Installazione

* Eseguire `composer install`
* Copiare il file `config.sample.php` in `config.php`
* Richiede un file `opml.xml` nelle cartelle `ils` e `lug`
  * Scaricando i file dalla produzione il token di ILS Manager non è necessario
    * https://planet.linux.it/ils/opml.xml (Dalla root del progetto puoi usare il comando `wget -P ils/ https://planet.linux.it/ils/opml.xml`)
    * https://planet.linux.it/lug/opml.xml (Dalla root del progetto puoi usare il comando `wget -P lug/ https://planet.linux.it/lug/opml.xml`)
* Eseguire `generate_contents.php` per generare i file

Per Meetup.com sono richiesti i cookie in un file `cookies-meetup-com.txt` da mettere in root di un utente loggato, sufficiente il cookie `MEETUP_MEMBER`. Si può esportare il file con una estensione tipo `Export Cookie` per il dominio e eliminare gli altri.

### Ordinare il file calendars.txt

Per non dover ordinare lato codice il file è necessario eseguire questo comando:

```shell
$ cat eventi/calendars.txt | (read -r; printf "%s\n" "$REPLY"; sort)
```
