<?php

if (!in_array(PHP_SAPI, ['cli', 'phpdbg', 'embed'], true)) {
    echo 'Error: The console should be invoked via the CLI version of PHP, not the '.PHP_SAPI.' SAPI'.PHP_EOL;
    die();
}

require_once('funzioni.php');

$path = 'lug/opml.xml';
generateLugOpml($path);

$path = 'ils/opml.xml';
generateIlsOpml($path);

